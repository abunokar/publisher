json.extract! publication, :id, :title, :release_date, :abstract, :person_id, :created_at, :updated_at
json.url publication_url(publication, format: :json)
