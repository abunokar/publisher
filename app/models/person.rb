class Person < ApplicationRecord
  belongs_to :faculty
  has_many :publications

  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
end
