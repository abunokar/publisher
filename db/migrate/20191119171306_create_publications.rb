class CreatePublications < ActiveRecord::Migration[6.0]
  def change
    create_table :publications do |t|
      t.string :title
      t.date :release_date
      t.text :abstract
      t.references :person, null: false, foreign_key: true

      t.timestamps
    end
  end
end
